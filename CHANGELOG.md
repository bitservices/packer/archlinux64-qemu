<!----------------------------------------------------------------------------->

### **2.0.13**
* **Archlinux Package Level**: 11 February 2025
* **Archlinux Boot ISO Used**: archlinux-2025.02.01-dual.iso

<!----------------------------------------------------------------------------->

### **2.0.12**
* **Archlinux Package Level**: 16 January 2025
* **Archlinux Boot ISO Used**: archlinux-2025.01.01-dual.iso
* **Change**: Update pacman initial mirrorlist (version: 2025-01-01). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.11**
* **Archlinux Package Level**: 8 November 2024
* **Archlinux Boot ISO Used**: archlinux-2024.11.01-dual.iso
* **Add**: Enable service unit `cloud-init-main.service`. - [Rich Lees]
* **Add**: Enable service unit `cloud-init-network.service`. - [Rich Lees]
* **Remove**: No longer enable deprecated service unit `cloud-init.service`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.10**
* **Archlinux Package Level**: 1 October 2024
* **Archlinux Boot ISO Used**: archlinux-2024.10.01-dual.iso

<!----------------------------------------------------------------------------->

### **2.0.9**
* **Archlinux Package Level**: 1 September 2024
* **Archlinux Boot ISO Used**: archlinux-2024.09.01-dual.iso
* **Add**: Add package `cloud-guest-utils` to get built in support for partition resizing. - [Rich Lees]
* **Add**: Add **growpart** `cloud-init` module. - [Rich Lees]
* **Change**: Migrated to Hashicorp HCP for Vagrant box hosting. New tag is: `bitservices/archlinux64`. - [Rich Lees]
* **Remove**: Remove package `grow-root-part` as it should no longer be needed. - [Rich Lees]
* **Remove**: Remove **growrootpart** hook from `mkinitcpio.conf`. - [Rich Lees]
* **Remove**: Remove **migrator** `cloud-init` module. - [Rich Lees]

* **IMPORTANT**: For Vagrant box, please use: `bitservices/archlinux64` instead.

<!----------------------------------------------------------------------------->

### **2.0.8**
* **Archlinux Package Level**: 12 August 2024
* **Archlinux Boot ISO Used**: archlinux-2024.08.01-dual.iso
* **Change**: Update pacman initial mirrorlist (version: 2024-07-17). - [Rich Lees]
* **Bugfix**: The mirrorlist region is now being set correctly - attempt 2. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.7**
* **Archlinux Package Level**: 21 June 2024
* **Archlinux Boot ISO Used**: archlinux-2024.06.01-dual.iso

<!----------------------------------------------------------------------------->

### **2.0.6**
* **Archlinux Package Level**: 1 June 2024
* **Archlinux Boot ISO Used**: archlinux-2024.06.01-dual.iso

<!----------------------------------------------------------------------------->

### **2.0.5**
* **Archlinux Package Level**: 1 April 2024
* **Archlinux Boot ISO Used**: archlinux-2024.04.01-dual.iso
* **Change**: Management of **makepkg** settings now done by `bitservices-core`. - [Rich Lees]

* **NOTE**: Includes fixes and mitigation for:
[https://archlinux.org/news/the-xz-package-has-been-backdoored/](https://archlinux.org/news/the-xz-package-has-been-backdoored/)

<!----------------------------------------------------------------------------->

### **2.0.4**
* **Archlinux Package Level**: 20 March 2024
* **Archlinux Boot ISO Used**: archlinux-2024.03.01-dual.iso
* **Change**: Change **makepkg** option `debug` to `!debug`. - [Rich Lees]
* **Change**: Stop **makepkg** from compressing packages. - [Rich Lees]
* **Change**: Update **mkinitcpio** configuration files to latest. - [Rich Lees]
* **Bugfix**: The mirrorlist region is now being set correctly. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.3**
* **Archlinux Package Level**: 1 March 2024
* **Archlinux Boot ISO Used**: archlinux-2024.02.01-dual.iso

<!----------------------------------------------------------------------------->

### **2.0.2**
* **Archlinux Package Level**: 27 October 2023
* **Archlinux Boot ISO Used**: archlinux-2023.10.14-dual.iso
* **Change**: Builder machine now uses `5120` megabytes of memory when running in CI. - [Rich Lees]
* **Change**: Update pacman initial mirrorlist (version: 2023-10-01). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.1**
* **Archlinux Package Level**: 4 August 2023
* **Archlinux Boot ISO Used**: archlinux-2023.08.01-dual.iso
* **Change**: Update pacman initial mirrorlist (version: 2023-06-28). - [Rich Lees]
* **Change**: Builder machine now uses `TCG` acceleration when running in CI. - [Rich Lees]
* **Change**: Builder machine now uses `qemu64` CPU when running in CI. - [Rich Lees]
* **Change**: Builder machine now uses `2` CPUs. - [Rich Lees]
* **Change**: Builder machine now uses `2048` megabytes of memory (`10240` when running in CI). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **2.0.0**
* **Archlinux Package Level**: 25 July 2023
* **Archlinux Boot ISO Used**: archlinux-2023.07.01-dual.iso
* **Add**: Vagrant box for the `libvirt` provider. - [Rich Lees]
* **Add**: Do not pacstrap during build until **Pacman** is fully initialised with a `300` second timeout. - [Rich Lees]
* **Change**: Use **QEMU** as the base builder instead of **VirtualBox**. - [Rich Lees]
* **Change**: Builder machine uses `Q35` chipset instead of `PIIX3`. - [Rich Lees]
* **Change**: Builder machine uses `virtio` controller for storage instead of `sata`. - [Rich Lees]
* **Change**: Builder machine uses `writeback` caching for storage. - [Rich Lees]
* **Change**: Builder machine uses compression for storage. - [Rich Lees]
* **Change**: Builder machine uses `qcow2` format for storage. - [Rich Lees]
* **Change**: Builder machine uses `virtio-net` in `user` mode for networking. - [Rich Lees]
* **Change**: Builder machine uses `virtio` display adapter. - [Rich Lees]
* **Change**: Builder machine has the TPM explicitly disabled. - [Rich Lees]
* **Change**: Builder machine uses the QEMU defaults for audio. - [Rich Lees]
* **Change**: Builder machine uses the QEMU defaults for USB. - [Rich Lees]
* **Change**: Use `zstd` instead of `lzo` for **BTRFS** compression. - [Rich Lees]
* **Remove**: NoCloud cloud-init data no longer provided with Vagrant box. You MUST provide one yourself or login won't be possible! - [Rich Lees]
* **Remove**: Vagrant box for the `virtualbox` provider. - [Rich Lees]
* **Remove**: Remove `virtualbox-guest-utils-nox` package from Vagrant box. - [Rich Lees]
* **Remove**: NetworkManager exception to `enp0s8` on the Vagrant box. NetworkManager will now manage all interfaces. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **1.0.3**
* **Archlinux Package Level**: 24 June 2023
* **Archlinux Boot ISO Used**: archlinux-2023.06.01-dual.iso
* **Change**: Use `bitservices-core` package to manage some previously manually managed settings. - [Rich Lees]
* **Add**: Add **reflector** pacman hook to refresh mirror lists automatically. - [Rich Lees]
* **Change**: Refresh `linux.preset` file for **mkinitcpio**. No tangible change. - [Rich Lees]
* **Change**: Update pacman initial mirrorlist (version: 2023-04-10). - [Rich Lees]
* **Change**: Refresh `pacman.conf`. No tangible change. - [Rich Lees]
* **Add**: Retain the 'kms' hook in `mkinitcpio.conf` since GPU instances might need it. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **1.0.2**
* **Archlinux Package Level**: 13 February 2023
* **Archlinux Boot ISO Used**: archlinux-2023.02.01-dual.iso

<!----------------------------------------------------------------------------->

### **1.0.1**
* **Archlinux Package Level**: 4 December 2022
* **Archlinux Boot ISO Used**: archlinux-2022.12.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2022-12-04). - [Rich Lees]
* **Change**: Update `mkinitcpio.conf` from latest Archlinux. - [Rich Lees]
* **Change**: Update `cloud.cfg` from latest Archlinux. - [Rich Lees]
* **Add**: Add file `/etc/os-release` to **mkinitcpio** files. - [Rich Lees]
* **Add**: Add **migrator** `cloud-init` module. - [Rich Lees]
* **Add**: Add **seed_random** `cloud-init` module. - [Rich Lees]
* **Add**: Add **mounts** `cloud-init` module. - [Rich Lees]
* **Add**: Add **uucp** to the default users groups using `cloud-init`. - [Rich Lees]
* **Change**: Change **datasource_list** format in `cloud.cfg` for increased compatibility. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **1.0.0**
* **Archlinux Package Level**: 2 November 2022
* **Archlinux Boot ISO Used**: archlinux-2022.11.01-dual.iso
* **Add**: Add `discard` mount option for disks during provisioning. - [Rich Lees]
* **Add**: Add `ssd` mount option for disks during provisioning (BTRFS only). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.5.0**
* **Archlinux Package Level**: 2 November 2022
* **Archlinux Boot ISO Used**: archlinux-2022.11.01-dual.iso
* **Add**: Add `-K` parameter to **pacstrap** as per Arch Linux installation guide. - [Rich Lees]
* **Change**: Use HCL Packer scripts instead of JSON. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2022-10-16). - [Rich Lees]
* **Change**: Use **64** megabytes of video RAM during provisioning. -[Rich Lees]
* **Change**: Guest audio controller is now **ac97** since none is not an option. - [Rich Lees]
* **Change**: Use **boxsvga** video controller for provisioning without 3D acceleration. - [Rich Lees]
* **Change**: Configure hard disk used for provisioning as an SSD with **discard** enabled. - [Rich Lees]
* **Remove**: Remove step to fill disk with zeros since we are using **discard** mode now. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.4.1**
* **Archlinux Package Level**: 10 January 2022
* **Archlinux Boot ISO Used**: archlinux-2022.01.01-dual.iso
* **Add**: Add a `partprobe` step after creating partitions and file systems since this no longer seems to be updated automatically. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.4.0**
* **Archlinux Package Level**: 10 January 2022
* **Archlinux Boot ISO Used**: archlinux-2022.01.01-dual.iso
* **Add**: Set `match` directive to match all original network interfaces in `99-default.link`. - [Rich Lees]
* **Add**: Set `NamePolicy` directive in `99-default.link` to match Archlinux defaults. - [Rich Lees]
* **Add**: Set `AlternativeNamesPolicy` directive in `99-default.link` to match Archlinux defaults. - [Rich Lees]
* **Change**: NetworkManager now manages all network interfaces (except `enp0s8` on the Vagrant box build). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.7**
* **Archlinux Package Level**: 10 January 2022
* **Archlinux Boot ISO Used**: archlinux-2022.01.01-dual.iso
* **Change**: Configure `cloud-init` to only generate an `ed25519` SSH host key. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.6**
* **Archlinux Package Level**: 10 January 2022
* **Archlinux Boot ISO Used**: archlinux-2022.01.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2021-12-12). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.5**
* **Archlinux Package Level**: 01 October 2021
* **Archlinux Boot ISO Used**: archlinux-2021.09.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2021-08-22). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.4**
* **Archlinux Package Level**: 13 July 2021
* **Archlinux Boot ISO Used**: archlinux-2021.07.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2021-05-09). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.3**
* **Archlinux Package Level**: 01 June 2021
* **Archlinux Boot ISO Used**: archlinux-2021.06.01-dual.iso
* **Add**: Enable pacman option `ParallelDownloads` and set the limit to `3` since any more would kill most DSL connections. - [Rich Lees]
* **Remove**: Remove pacman option `TotalDownload` as it no longer exists for pacman 6. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.2**
* **Archlinux Package Level**: 17 May 2021
* **Archlinux Boot ISO Used**: archlinux-2021.05.01-dual.iso
* **Remove**: Remove package `virtualbox-guest-dkms` from Vagrant box as it no longer exists. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.1**
* **Archlinux Package Level**: 20 April 2021
* **Archlinux Boot ISO Used**: archlinux-2021.04.01-dual.iso
* **New**: Trust BITServices packaging singing key `7FC5C97F4044C19A` (bitservices-packages.asc). - [Rich Lees]
* **New**: Add `bitservices` repo to pacman. - [Rich Lees]
* **New**: Add `pretty-motd` bitservices package. - [Rich Lees]
* **Change**: Enable pacman options `Color` and `TotalDownload`. - [Rich Lees]
* **Change**: Install `grow-root-part` from the bitservices repo rather than as a manual download. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.3.0**
* **Archlinux Package Level**: 17 April 2021
* **Archlinux Boot ISO Used**: archlinux-2021.04.01-dual.iso
* **New**: NTP is now enabled using `systemd-timesyncd`. - [Rich Lees]
* **New**: Configure `cloud-init` to lock the default users password. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2021-04-05). - [Rich Lees]
* **Change**: Use the `kyber` scheduler for non-rotational NVMe disks. - [Rich Lees]
* **Change**: Use `CIDATA` instead of `cidata` as the label for the NoCloud volume used by the Vagrant box. - [Rich Lees]
* **Remove**: Remove package: `netctl` due to it no longer being required for networking. - [Rich Lees]
* **Remove**: Remove package: `python-pyopenssl` as Ansible working out the box is no longer a project aim. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.27**
* **Archlinux Package Level**: 17 June 2020
* **Archlinux Boot ISO Used**: archlinux-2020.06.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.26**
* **Archlinux Package Level**: 06 April 2020
* **Archlinux Boot ISO Used**: archlinux-2020.04.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.25**
* **Archlinux Package Level**: 24 February 2020
* **Archlinux Boot ISO Used**: archlinux-2020.02.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2020-02-07). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.24**
* **Archlinux Package Level**: 06 January 2020
* **Archlinux Boot ISO Used**: archlinux-2020.01.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2020-01-03). - [Rich Lees]
* **Bugfix**: Comment out United Kingdom mirrors so only worldwide mirrors are selected. - [Rich Lees]
* **Change**: Use official `cloud-init` package now that it is available again. - [Rich Lees]
* **New**: Include the following packages following the Arch Linux **base** meta-package change: `inetutils`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.23**
* **Archlinux Package Level**: 13 October 2019
* **Archlinux Boot ISO Used**: archlinux-2019.10.01-dual.iso
* **New**: Include the following packages following the Arch Linux **base** meta-package change: `linux`, `linux-firmware`, `logrotate`, `netctl`. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2019-10-01). - [Rich Lees]
* **Change**: Only use **https** enabled **worldwide** mirrors. - [Rich Lees]
* **Remove**: No longer disable `sshd.socket` as it no longer ships with Arch Linux. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.22**
* **Archlinux Package Level**: 10 September 2019
* **Archlinux Boot ISO Used**: archlinux-2019.09.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2019-08-21). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.21**
* **Archlinux Package Level**: 04 August 2019
* **Archlinux Boot ISO Used**: archlinux-2019.08.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.20**
* **Archlinux Package Level**: 19 July 2019
* **Archlinux Boot ISO Used**: archlinux-2019.07.01-dual.iso
* **New**: Include `pacman-contrib` package. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2019-06-14). - [Rich Lees]
* **Remove**: No longer use custom `journald.conf` with custom **SystemMaxUse** setting. - [Rich Lees]
* **Remove**: No longer use custom `cloud-init.service`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.19**
* **Archlinux Package Level**: 10 May 2019
* **Archlinux Boot ISO Used**: archlinux-2019.05.02-dual.iso
* **New**: Include `python-pyopenssl` package. - [Rich Lees]
* **Remove**: Remove `python2` package. - [Rich Lees]
* **Remove**: Remove `python2-pyopenssl` package. - [Rich Lees]
* **New**: Add `cloud-init` module **update_hostname** to run during the **init** stage. - [Rich Lees]
* **Change**: Change `cloud-init` configuration **Preserve Hostname** to **Disabled**. - [Rich Lees]
* **Change**: Change `cloud-init` configuration **SSH Service Name** to **sshd**. - [Rich Lees]
* **Change**: Change `cloud-init` default user group membership to include **adm**, **audio**, **floppy** and **ideo**. - [Rich Lees]
* **Change**: Remove `cloud-init` logging override configuration and just use defaults. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.18**
* **Archlinux Package Level**: 02 April 2019
* **Archlinux Boot ISO Used**: archlinux-2019.04.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.17**
* **Archlinux Package Level**: 06 March 2019
* **Archlinux Boot ISO Used**: archlinux-2019.03.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.16**
* **Archlinux Package Level**: 12 February 2019
* **Archlinux Boot ISO Used**: archlinux-2019.02.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2019-01-09). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.15**
* **Archlinux Package Level**: 02 January 2019
* **Archlinux Boot ISO Used**: archlinux-2019.01.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.14**
* **Archlinux Package Level**: 24 December 2018
* **Archlinux Boot ISO Used**: archlinux-2018.12.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2018-12-05). - [Rich Lees]
* **Change**: Replace `growfs` files/hook with `grow-root-part` external package. - [Rich Lees]
* **Change**: Add `systemd`, `sd-vconsole`, `growrootpart` hooks and remove `udev`, `keymap`, `growfs` hooks from initramfs. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.13**
* **Archlinux Package Level**: 01 December 2018
* **Archlinux Boot ISO Used**: archlinux-2018.12.01-dual.iso
* **Change**: `mkfs.vfat` now requires the **-I** parameter to create the Vagrant NoCloud configuration disk in superfloppy mode. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.12**
* **Archlinux Package Level**: 13 October 2018
* **Archlinux Boot ISO Used**: archlinux-2018.10.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2018-09-12). - [Rich Lees]
* **Change**: Use the worldwide mirrors. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.11**
* **Archlinux Package Level**: 01 September 2018
* **Archlinux Boot ISO Used**: archlinux-2018.09.01-dual.iso
* **Bugfix**: Fix `growfs` hook to work with NVMe devices. - [Rich Lees]
* **Change**: Add `keymap` hook and remove `shutdown` hook from initramfs. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.10**
* **Archlinux Package Level**: 04 July 2018
* **Archlinux Boot ISO Used**: archlinux-2018.07.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.2.9**
* **Archlinux Package Level**: 02 June 2018
* **Archlinux Boot ISO Used**: archlinux-2018.06.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2018-05-24). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.8**
* **Archlinux Package Level**: 08 May 2018
* **Archlinux Boot ISO Used**: archlinux-2018.05.01-dual.iso
* **New**: Include `python2-pyopenssl` package to allow **OpenSSL** modules in **Ansible**. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2018-04-15). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.7**
* **Archlinux Package Level**: 09 April 2018
* **Archlinux Boot ISO Used**: archlinux-2018.04.01-dual.iso
* **New**: Include **Network Manager** to manage **eth0** (allows for IPv6). - [Rich Lees]
* **New**: **BTRFS** root volumes are checked with `btrfs check` at build time. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.6**
* **Archlinux Package Level**: 03 April 2018
* **Archlinux Boot ISO Used**: archlinux-2018.04.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2018-02-24). - [Rich Lees]
* **Change**: Update the **growpart** binary. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.5**
* **Archlinux Package Level**: 06 March 2018
* **Archlinux Boot ISO Used**: archlinux-2018.03.01-dual.iso
* **New**: Set `vm.swappiness` to **25** on all images. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.4**
* **Archlinux Package Level**: 02 February 2018
* **Archlinux Boot ISO Used**: archlinux-2018.02.01-dual.iso
* **Change**: Switch top two UK mirrors due to recent issues with bytemark. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.3**
* **Archlinux Package Level**: 02 January 2018
* **Archlinux Boot ISO Used**: archlinux-2018.01.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2017-12-03). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.2**
* **Archlinux Package Level**: 02 December 2017
* **Archlinux Boot ISO Used**: archlinux-2017.12.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2017-10-27). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.1**
* **Archlinux Package Level**: 01 November 2017
* **Archlinux Boot ISO Used**: archlinux-2017.11.01-dual.iso
* **Change**: Update `mkinitcpio.conf` format. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.2.0**
* **Archlinux Package Level**: 01 October 2017
* **Archlinux Boot ISO Used**: archlinux-2017.10.01-dual.iso
* **New**: `cloud-init` is now included. See note below. - [Rich Lees]
* **New**: The Vagrant box now includes a 10MB NoCloud configuration disk. This may break existing scripts that expect to see any user added disks on `/dev/sdb`. - [Rich Lees]
* **New**: Inclusion of customised `growfs` mkinitcpio hook and supporting files to resize partitions (not filesystems) on boot. - [Rich Lees]
* **Change**: The default SSH username is now: `archlinux`. - [Rich Lees]
* **Change**: Default user (`archlinux`) configuration is now done entirely by `cloud-init`. - [Rich Lees]
* **Change**: Network interfaces now use old kernel names such as `eth0`. - [Rich Lees]
* **Change**: Network MAC address policy is now `none` which uses kernel defaults. - [Rich Lees]
* **Change**: Netctl profile for default network interface (`eth0`) is now called `Default NAT`. - [Rich Lees]
* **Change**: SSHD password authentication is now disabled. - [Rich Lees]
* **Change**: The SATA controller now has a total of `6` ports. - [Rich Lees]
* **Change**: Update pacman mirrorlist (version: 2017-09-07). - [Rich Lees]

**Note**: `cloud-init` that is now included has the Archlinux default configuration with the following changes:
* **Disable Root**: Enabled
* **Preserve Hostname**: Enabled
* **Resize Root Filesystem**: Enabled
* **SSH Password Auth**: Disabled
* **Generated SSH Host Key Types**: ed25519, rsa, ecdsa, dsa
* **Distribution**: arch
* **Default User**: archlinux
    * **Friendly Name**: Archlinux
    * **Groups**: wheel, users
    * **Sudo**: ALL=(ALL) NOPASSWD:ALL
    * **Shell**: /bin/bash
* **Modules**:
    * **Init**: resizefs
    * **Init**: bootcmd
    * **Init**: write-files
    * **Init**: set_hostname
    * **Init**: users-groups
    * **Init**: ssh
    * **Config**: set_passwords
    * **Config**: runcmd
    * **Final**: rightscale_userdata
    * **Final**: scripts-per-once
    * **Final**: scripts-per-boot
    * **Final**: scripts-per-instance
    * **Final**: scripts-user
    * **Final**: ssh-authkey-fingerprints
    * **Final**: keys-to-console
    * **Final**: final-message
* **Networking**: Management by cloud-init disabled
* **Datasources**: NoCloud, Ec2, None
* **Logging**: To file `/var/log/cloud-init.log`, level `INFO`
* **Output**: To file `/var/log/cloud-init-output.log`, level `INFO`

<!----------------------------------------------------------------------------->

### **0.1.8**
* **Archlinux Package Level**: 01 September 2017
* **Archlinux Boot ISO Used**: archlinux-2017.09.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2017-07-29). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.1.7**
* **Archlinux Package Level**: 01 August 2017
* **Archlinux Boot ISO Used**: archlinux-2017.08.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2017-07-14). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.1.6**
* **Archlinux Package Level**: 01 July 2017
* **Archlinux Boot ISO Used**: archlinux-2017.07.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.1.5**
* **Archlinux Package Level**: 01 June 2017
* **Archlinux Boot ISO Used**: archlinux-2017.06.01-dual.iso
* **Change**: Update pacman mirrorlist (version: 2017-04-27). - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.1.4**
* **Archlinux Package Level**: 06 May 2017
* **Archlinux Boot ISO Used**: archlinux-2017.05.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.1.3**
* **Archlinux Package Level**: 03 April 2017
* **Archlinux Boot ISO Used**: archlinux-2017.04.01-dual.iso
* **New**: The installer scripts can now be configured to choose the root filesystem type. Supported options for now are `BTRFS` and `EXT4`. - [Rich Lees]
* **Change**: The default root filesystem type for the Vagrant box is now `BTRFS` with root `@` and home `@home` subvolumes. - [Rich Lees]
* **Change**: Include `f2fs-tools` package in the Vagrant box. - [Rich Lees]
* **Change**: Packer script uses `SHA1` instead of `SHA256` to verify checksums due to the `SHA1` sum being available on the Archlinux downloads page. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.1.2**
* **Archlinux Package Level**: 01 March 2017
* **Archlinux Boot ISO Used**: archlinux-2017.03.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.1.1**
* **Archlinux Package Level**: 02 February 2017
* **Archlinux Boot ISO Used**: archlinux-2017.02.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.1.0**
* **Archlinux Package Level**: 01 January 2017
* **Archlinux Boot ISO Used**: archlinux-2017.01.01-dual.iso
* **New**: Use of **Packer** https://www.packer.io/ to automate creation of this Vagrant box. This will cause subtle changes to previous releases such as the file name of the included primary hard disk VMDK image. - [Rich Lees]
* **New**: Ensure the cable is marked as connected to the NAT virtual NIC. - [arivarton]
* **Change**: The firmware type is now explicitly set to `BIOS` whereas before the VirtualBox default was used. - [Rich Lees]
* **Change**: Chipset is now explicitly set to `PIIX3`. - [Rich Lees]
* **Change**: ACPI is now explicitly `enabled`. - [Rich Lees]
* **Change**: I/O APIC is now explicitly `enabled`. - [Rich Lees]
* **Change**: CPU Hotplugging is now explicitly `disabled`. This allows easier automatic customisation by users of this Vagrant box by using a `Vagrantfile`. - [Rich Lees]
* **Change**: RTC (clock) use UTC is now explicitly `enabled`. - [Rich Lees]
* **Change**: Video Capture is now explicitly `disabled`. - [Rich Lees]
* **Change**: Video Remote Display is now explicitly `disabled`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.0.3**
* **Archlinux Package Level**: 02 December 2016
* **Archlinux Boot ISO Used**: archlinux-2016.12.01-dual.iso

<!----------------------------------------------------------------------------->

### **0.0.2**
* **Archlinux Package Level**: 18 November 2016
* **Archlinux Boot ISO Used**: archlinux-2016.11.01-dual.iso
* **New**: Box comes with cleared pacman cache to reduce box size. - [Rich Lees]
* **Change**: Change name of floppy disk controller to: `FDD Controller`. - [Rich Lees]
* **Change**: Change name of optical disk IDE controller to: `ODD Controller`. - [Rich Lees]

<!----------------------------------------------------------------------------->

### **0.0.1**
* **Archlinux Package Level**: 17 November 2016
* **Archlinux Boot ISO Used**: archlinux-2016.11.01-dual.iso
* **New**: rlees85/archlinux64 Vagrant box initial release. - [Rich Lees]

<!----------------------------------------------------------------------------->

[Rich Lees]: https://gitlab.com/rlees85
[arivarton]: https://gitlab.com/arivarton
