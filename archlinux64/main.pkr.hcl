###############################################################################

variable "output"  {}
variable "release" {}

###############################################################################

variable "ec2_role"   {}
variable "ec2_bucket" {}

###############################################################################

variable "name" { default = "archlinux64" }

###############################################################################

variable "changelog_path" { default = "-/blob/latest/CHANGELOG.md"                             }
variable "changelog_repo" { default = "https://gitlab.com/bitservices/packer/archlinux64-qemu" }

###############################################################################

variable "ec2_class"             { default = "ec2" }
variable "ec2_storage_root_size" { default = "3G"  }

###############################################################################

variable "provision_settings_path"  { default = "/tmp/packer_settings"  }
variable "provision_templates_path" { default = "/tmp/packer_templates" }

###############################################################################

variable "proxy_host" { default = "" }
variable "proxy_port" { default = "" }

###############################################################################

variable "vagrant_keep"              { default = false         }
variable "vagrant_class"             { default = "vagrant"     }
variable "vagrant_registry"          { default = "bitservices" }
variable "vagrant_compression_level" { default = 9             }
variable "vagrant_storage_root_size" { default = "20G"         }

###############################################################################

locals {
  ec2_name            = format("%s-%s", var.name, var.ec2_class)
  ec2_output          = format("%s/%s/%s", var.output, var.name, var.ec2_class)
  ec2_source          = format("qemu.%s", var.ec2_class)
  vagrant_tag         = format("%s/%s", var.vagrant_registry, var.name)
  vagrant_file        = format("%s/%s.vagrantfile", path.root, var.name)
  vagrant_name        = format("%s-%s", var.name, var.vagrant_class)
  vagrant_output      = format("%s/%s/%s", var.output, var.name, var.vagrant_class)
  vagrant_source      = format("qemu.%s", var.vagrant_class)
  vagrant_description = format("%s/%s#%s", var.changelog_repo, var.changelog_path, replace(var.release, ".", ""))
}

###############################################################################

build {
  sources = [
    format("source.%s", local.ec2_source),
    format("source.%s", local.vagrant_source)
  ]

  provisioner "shell" {
    inline = [
      "passwd -l root"
    ]
  }

  provisioner "shell" {
    inline = [
      format("mkdir -p \"%s\"", var.provision_settings_path),
      format("mkdir -p \"%s\"", var.provision_templates_path)
    ]
  }

  provisioner "file"{
    source      = format("%s/settings/", path.root)
    destination = var.provision_settings_path
  }

  provisioner "file"{
    source      = format("%s/templates/", path.root)
    destination = var.provision_templates_path
  }

  provisioner "shell" {
    script          = format("%s/scripts/prepare.sh", path.root)
    execute_command = format("{{ .Path }} -s '%s'", var.provision_settings_path)
  }

  provisioner "shell" {
    script          = format("%s/scripts/common.sh", path.root)
    execute_command = format("{{ .Path }} -p '%s' -o '%s' -t '%s' -s '%s'", var.proxy_host, var.proxy_port, var.provision_templates_path, var.provision_settings_path)
  }

  provisioner "shell" {
    only            = [ local.ec2_source ]
    script          = format("%s/scripts/ec2.sh", path.root)
    execute_command = format("{{ .Path }} -p '%s' -o '%s' -t '%s' -s '%s'", var.proxy_host, var.proxy_port, var.provision_templates_path, var.provision_settings_path)
  }

  provisioner "shell" {
    script          = format("%s/scripts/cleanup.sh", path.root)
    execute_command = format("{{ .Path }} -s '%s'", var.provision_settings_path)
  }

  post-processors {
    post-processor "vagrant" {
      only                 = [ local.vagrant_source ]
      output               = format("%s/{{ .BuildName }}.box", local.vagrant_output)
      compression_level    = var.vagrant_compression_level
      keep_input_artifact  = var.vagrant_keep
      vagrantfile_template = local.vagrant_file
    }

    post-processor "vagrant-registry" {
      only                = [ local.vagrant_source ]
      box_tag             = local.vagrant_tag
      version             = var.release
      version_description = local.vagrant_description
    }

    post-processor "shell-local" {
      only            = [ local.ec2_source ]
      script          = format("%s/scripts/ec2-publish.sh", path.root)
      execute_command = [ format("{{ .Script }} -b '%s' -k '%s/%s/%s.qcow2' -r '%s' -s '%s/%s'", var.ec2_bucket, var.name, var.release, var.ec2_class, var.ec2_role, local.ec2_output, local.ec2_name) ]
    }
  }
}

###############################################################################
