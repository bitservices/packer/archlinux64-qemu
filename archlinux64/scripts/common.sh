#!/bin/bash -e
#
# A set of scripts to prepare a Virtual Machine to become an Archlinux image.
# Starting with just the Arch Linux ISO this set of scripts will install
# Arch Linux and make any required configuration to the image.
#
# Common stage: Tasks that apply to both cloud and nocloud images.
#
###############################################################################

set -o pipefail

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Arch Linux Cloud Image Preparation Script                            -"
echo "------------------------------------------------------------------------"
echo "- Common: Tasks Applicable to All Images                               -"
echo "------------------------------------------------------------------------"

PS_PROXY_HOST=""
PS_PROXY_PORT=""
PS_TEMPLATES_FOLDER=""
PS_SETTINGS_FOLDER=""

while getopts "p:o:t:s:" PS_OPT; do
  case ${PS_OPT} in
    p)
      PS_PROXY_HOST="${OPTARG}"
      ;;
    o)
      PS_PROXY_PORT="${OPTARG}"
      ;;
    t)
      PS_TEMPLATES_FOLDER="${OPTARG}"
      ;;
    s)
      PS_SETTINGS_FOLDER="${OPTARG}"
      ;;
  esac
done

if [ -z "${PS_TEMPLATES_FOLDER}"   ]; then echo "FAIL: No configuration templates folder specified (-t)"            ; exit 1; fi
if [ -z "${PS_SETTINGS_FOLDER}"    ]; then echo "FAIL: No configuration settings folder specified (-s)"             ; exit 1; fi
if [ ! -d "${PS_TEMPLATES_FOLDER}" ]; then echo "FAIL: Configuration templates folder specified (-t) does not exist"; exit 1; fi
if [ ! -d "${PS_SETTINGS_FOLDER}"  ]; then echo "FAIL: Configuration settings folder specified (-s) does not exist" ; exit 1; fi
if [ -n "${PS_PROXY_HOST}"         ]; then
if [ -n "${PS_PROXY_PORT}"         ]; then
  export PS_PROXY_URL="http://${PS_PROXY_HOST}:${PS_PROXY_PORT}"
  export http_proxy="${PS_PROXY_URL}"
  export https_proxy="${PS_PROXY_URL}"
  echo "Using Proxy            : ${PS_PROXY_URL}"
fi
fi

echo "Configuration Templates: ${PS_TEMPLATES_FOLDER}"
echo "Configuration Settings : ${PS_SETTINGS_FOLDER}"
echo ""

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Importing settings...                                                -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_HDD="${PS_SETTINGS_FOLDER}/hdd.vars"
PS_SETTINGS_LANG="${PS_SETTINGS_FOLDER}/lang.vars"
PS_SETTINGS_NETWORK="${PS_SETTINGS_FOLDER}/network.vars"
PS_SETTINGS_PACKAGES="${PS_SETTINGS_FOLDER}/packages.vars"

if [ ! -f "${PS_SETTINGS_HDD}"      ]; then echo "FAIL: Hard disk settings file could not be found!"; exit 1; else source "${PS_SETTINGS_HDD}"     ; fi
if [ ! -f "${PS_SETTINGS_LANG}"     ]; then echo "FAIL: Language settings file could not be found!" ; exit 1; else source "${PS_SETTINGS_LANG}"    ; fi
if [ ! -f "${PS_SETTINGS_NETWORK}"  ]; then echo "FAIL: Network settings file could not be found!"  ; exit 1; else source "${PS_SETTINGS_NETWORK}" ; fi
if [ ! -f "${PS_SETTINGS_PACKAGES}" ]; then echo "FAIL: Packages settings file could not be found!" ; exit 1; else source "${PS_SETTINGS_PACKAGES}"; fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Pacman initialisation...                                             -"
echo "------------------------------------------------------------------------"

while (! systemctl is-active --quiet pacman-init.service) && [ $PS_PACKAGES_PACMAN_INIT_TIMEOUT -gt 0 ]; do
  echo "Waiting 10 seconds for Pacman initialisation..."
  PS_PACKAGES_PACMAN_INIT_TIMEOUT=$(( $PS_PACKAGES_PACMAN_INIT_TIMEOUT - 10 ))
  sleep 10
done

systemctl is-active pacman-init.service

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Pacstrapping root...                                                 -"
echo "------------------------------------------------------------------------"

cp -f "${PS_TEMPLATES_FOLDER}/etc/pacman.d/mirrorlist" "/etc/pacman.d/mirrorlist"
pacstrap -K /mnt ${PS_PACKAGES_PACSTRAP}
sync

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Configuring Pacman...                                                -"
echo "------------------------------------------------------------------------"

cp -fv "${PS_TEMPLATES_FOLDER}/${PS_PACKAGES_BITSERVICES_KEY_FILE}" "/mnt/${PS_PACKAGES_BITSERVICES_KEY_FILE}"

arch-chroot /mnt pacman-key --add "/${PS_PACKAGES_BITSERVICES_KEY_FILE}"
arch-chroot /mnt pacman-key --lsign "${PS_PACKAGES_BITSERVICES_KEY_ID}"

rm -fv "/mnt/${PS_PACKAGES_BITSERVICES_KEY_FILE}"

cp -fv "${PS_TEMPLATES_FOLDER}/etc/pacman.conf" "/mnt/etc/pacman.conf"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- BITServices Packages...                                              -"
echo "------------------------------------------------------------------------"

arch-chroot /mnt pacman -Syu --needed --noconfirm ${PS_PACKAGES_BITSERVICES}

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Non-chroot config files...                                           -"
echo "------------------------------------------------------------------------"

genfstab -p -U "/mnt" >> "/mnt/etc/fstab"
echo "${PS_NETWORK_HOSTNAME}" > "/mnt/etc/hostname"
mkdir "/mnt/root/.config"
sed -i "/${PS_LANG_ID}/s/^#//g" "/mnt/etc/locale.gen"
echo "LANG=${PS_LANG_ID}" > "/mnt/etc/locale.conf"
echo "LANG=${PS_LANG_ID}" > "/mnt/root/.config/locale.conf"
echo "KEYMAP=${PS_LANG_KEYBOARD}" > "/mnt/etc/vconsole.conf"
echo "BITSERVICES_MIRRORLIST_COUNTRY=${PS_PACKAGES_REGION}" >> "/mnt/etc/environment"
sed -i '/compress/s/^#//g' "/mnt/etc/logrotate.conf"
rm -fv "/mnt/boot/initramfs-linux-fallback.img"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/systemd/network/99-default.link" "/mnt/etc/systemd/network/99-default.link"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/mkinitcpio.conf" "/mnt/etc/mkinitcpio.conf"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/mkinitcpio.d/linux.preset" "/mnt/etc/mkinitcpio.d/linux.preset"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/cloud/cloud.cfg" "/mnt/etc/cloud/cloud.cfg"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/conf.d/pretty-motd" "/mnt/etc/conf.d/pretty-motd"

cat <<EOF >"/mnt/etc/NetworkManager/NetworkManager.conf"
  [connection]
  ipv6.ip6-privacy=2
EOF

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Chroot configuration...                                              -"
echo "------------------------------------------------------------------------"

arch-chroot /mnt locale-gen
arch-chroot /mnt systemctl enable pretty-motd.service
arch-chroot /mnt systemctl enable sshd.service
arch-chroot /mnt systemctl enable systemd-timesyncd.service
arch-chroot /mnt systemctl enable NetworkManager.service
arch-chroot /mnt systemctl enable cloud-init-main.service
arch-chroot /mnt systemctl enable cloud-init-local.service
arch-chroot /mnt systemctl enable cloud-init-network.service
arch-chroot /mnt systemctl enable cloud-config.service
arch-chroot /mnt systemctl enable cloud-final.service
arch-chroot /mnt passwd -l root

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
