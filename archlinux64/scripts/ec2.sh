#!/bin/bash -e
#
# A set of scripts to prepare a Virtual Machine to become an Archlinux image.
# Starting with just the Arch Linux ISO this set of scripts will install
# Arch Linux and make any required configuration to the image.
#
# EC2 stage: EC2 specific tasks only.
#
###############################################################################

set -o pipefail

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Arch Linux Cloud Image Preparation Script                            -"
echo "------------------------------------------------------------------------"
echo "- EC2: Include EC2 specific tweaks                                     -"
echo "------------------------------------------------------------------------"

PS_TEMPLATES_FOLDER=""

while getopts "p:o:t:s:" PS_OPT; do
  case ${PS_OPT} in
    p)
      PS_PROXY_HOST="${OPTARG}"
      ;;
    o)
      PS_PROXY_PORT="${OPTARG}"
      ;;
    t)
      PS_TEMPLATES_FOLDER="${OPTARG}"
      ;;
    s)
      PS_SETTINGS_FOLDER="${OPTARG}"
      ;;
  esac
done

if [ -z "${PS_TEMPLATES_FOLDER}"   ]; then echo "FAIL: No configuration templates folder specified (-t)"            ; exit 1; fi
if [ ! -d "${PS_TEMPLATES_FOLDER}" ]; then echo "FAIL: Configuration templates folder specified (-t) does not exist"; exit 1; fi
if [ -z "${PS_SETTINGS_FOLDER}"    ]; then echo "FAIL: No configuration settings folder specified (-s)"              ; exit 1; fi
if [ ! -d "${PS_SETTINGS_FOLDER}"  ]; then echo "FAIL: Configuration settings folder specified (-s) does not exist"  ; exit 1; fi
if [ -n "${PS_PROXY_HOST}"         ]; then
if [ -n "${PS_PROXY_PORT}"         ]; then
  export PS_PROXY_URL="http://${PS_PROXY_HOST}:${PS_PROXY_PORT}"
  export http_proxy="${PS_PROXY_URL}"
  export https_proxy="${PS_PROXY_URL}"
  echo "Using Proxy            : ${PS_PROXY_URL}"
fi
fi

echo "Configuration Templates: ${PS_TEMPLATES_FOLDER}"
echo "Configuration Settings : ${PS_SETTINGS_FOLDER}"
echo ""

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Importing settings...                                                -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_EC2="${PS_SETTINGS_FOLDER}/ec2.vars"

if [ ! -f "${PS_SETTINGS_EC2}" ]; then echo "FAIL: EC2 settings file could not be found!"; exit 1; else source "${PS_SETTINGS_EC2}"; fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- EC2 Packages                                                         -"
echo "------------------------------------------------------------------------"

arch-chroot /mnt pacman -S --needed --noconfirm ${PS_EC2_PACKAGES}

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Enable EBS Friendly Names for NVMe...                                -"
echo "------------------------------------------------------------------------"

cp -fv "${PS_TEMPLATES_FOLDER}/usr/local/bin/ec2nvme-nsid" "/mnt/usr/local/bin/ec2nvme-nsid"
cp -fv "${PS_TEMPLATES_FOLDER}/usr/local/bin/ebsnvme-id" "/mnt/usr/local/bin/ebsnvme-id"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/udev/rules.d/70-ec2-nvme-devices.rules" "/mnt/etc/udev/rules.d/70-ec2-nvme-devices.rules"

chmod 755 "/mnt/usr/local/bin/ec2nvme-nsid"
chmod 755 "/mnt/usr/local/bin/ebsnvme-id"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- EC2 non-chroot config files                                          -"
echo "------------------------------------------------------------------------"

mkdir "/mnt/etc/systemd/timesyncd.conf.d"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/conf.d/prometheus-node-exporter" "/mnt/etc/conf.d/prometheus-node-exporter"
cp -fv "${PS_TEMPLATES_FOLDER}/etc/systemd/timesyncd.conf.d/ntp-ec2.conf" "/mnt/etc/systemd/timesyncd.conf.d/ntp-ec2.conf"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- EC2 Chroot configuration...                                          -"
echo "------------------------------------------------------------------------"

arch-chroot /mnt systemctl enable prometheus-node-exporter.service

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
