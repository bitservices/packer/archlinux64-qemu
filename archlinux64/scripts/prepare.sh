#!/bin/bash -e
#
# A set of scripts to prepare a Virtual Machine to become an Archlinux image.
# Starting with just the Arch Linux ISO this set of scripts will install
# Arch Linux and make any required configuration to the image.
#
# Preparation stage: Partition, format and mount primary filesystem(s).
#
###############################################################################

set -o pipefail

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Arch Linux Cloud Image Preparation Script                            -"
echo "------------------------------------------------------------------------"
echo "- Prepare: Partition, Format and Mount                                 -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_FOLDER=""

while getopts "s:" PS_OPT; do
  case ${PS_OPT} in
    s)
      PS_SETTINGS_FOLDER="${OPTARG}"
      ;;
  esac
done

if [ -z "${PS_SETTINGS_FOLDER}"   ]; then echo "FAIL: No configuration settings folder specified (-s)"            ; exit 1; fi
if [ ! -d "${PS_SETTINGS_FOLDER}" ]; then echo "FAIL: Configuration settings folder specified (-s) does not exist"; exit 1; fi

echo "Configuration Settings: ${PS_SETTINGS_FOLDER}"
echo ""

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Importing settings...                                                -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_HDD="${PS_SETTINGS_FOLDER}/hdd.vars"

if [ ! -f "${PS_SETTINGS_HDD}" ]; then echo "FAIL: Hard disk settings file could not be found!"; exit 1; else source "${PS_SETTINGS_HDD}"; fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Partitioning disk...                                                 -"
echo "------------------------------------------------------------------------"

! mount | grep "${PS_HDD}" > /dev/null
PS_HDD_IS_MOUNTED=${?}

if [ ${PS_HDD_IS_MOUNTED} -eq 1 ]; then
  echo "ERROR! The target device is mounted!"
  exit 1
fi

sfdisk --delete "${PS_HDD}" || true
echo ',,83,*' | sfdisk "${PS_HDD}"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Create filesystem and mount partition...                             -"
echo "------------------------------------------------------------------------"
echo "Root Filesystem: ${PS_HDD_FILESYSTEM}"
echo ""

if [ "${PS_HDD_FILESYSTEM,,}" == "btrfs" ]; then
  mkfs.btrfs --force --label "${PS_HDD_LABEL}" ${PS_HDD_PARTITION}
  mount -o space_cache=${PS_HDD_BTRFS_SPACE_CACHE},compress=${PS_HDD_BTRFS_COMPRESSION},discard,ssd ${PS_HDD_PARTITION} /mnt
  btrfs subvolume create /mnt/@
  btrfs subvolume create /mnt/@home
  umount /mnt
  mount -o space_cache=${PS_HDD_BTRFS_SPACE_CACHE},compress=${PS_HDD_BTRFS_COMPRESSION},discard,ssd,subvol=@ ${PS_HDD_PARTITION} /mnt
  mkdir /mnt/home
  mount -o space_cache=${PS_HDD_BTRFS_SPACE_CACHE},compress=${PS_HDD_BTRFS_COMPRESSION},discard,ssd,subvol=@home ${PS_HDD_PARTITION} /mnt/home
elif [ "${PS_HDD_FILESYSTEM,,}" == "ext4" ]; then
  mkfs.ext4 -F -L "${PS_HDD_LABEL}" ${PS_HDD_PARTITION}
  tune2fs -i ${PS_HDD_EXT4_FSCK_INTERVAL} ${PS_HDD_PARTITION}
  mount -o discard ${PS_HDD_PARTITION} /mnt
else
  echo "ERROR! Unknown filesystem: ${PS_HDD_FILESYSTEM}"
  exit 1
fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Refreshing partitions and filesystems...                             -"
echo "------------------------------------------------------------------------"

sync
partprobe --summary "${PS_HDD}"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
