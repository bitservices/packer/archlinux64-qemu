#!/bin/bash -e
#
# A set of scripts to prepare a Virtual Machine to become an Archlinux image.
# Starting with just the Arch Linux ISO this set of scripts will install
# Arch Linux and make any required configuration to the image.
#
# Cleanup stage: Unmount, cleanup, zero free space, etc
#
###############################################################################

set -o pipefail

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Arch Linux Cloud Image Preparation Script                            -"
echo "------------------------------------------------------------------------"
echo "- Cleanup: Clean / Check the Final Image                               -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_FOLDER=""

while getopts "s:" PS_OPT; do
  case ${PS_OPT} in
    s)
      PS_SETTINGS_FOLDER="${OPTARG}"
      ;;
  esac
done

if [ -z "${PS_SETTINGS_FOLDER}"   ]; then echo "FAIL: No configuration settings folder specified (-s)"            ; exit 1; fi
if [ ! -d "${PS_SETTINGS_FOLDER}" ]; then echo "FAIL: Configuration settings folder specified (-s) does not exist"; exit 1; fi

echo "Configuration Settings: ${PS_SETTINGS_FOLDER}"
echo ""

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Importing settings...                                                -"
echo "------------------------------------------------------------------------"

PS_SETTINGS_HDD="${PS_SETTINGS_FOLDER}/hdd.vars"

if [ ! -f "${PS_SETTINGS_HDD}" ]; then echo "FAIL: Hard disk settings file could not be found!"; exit 1; else source "${PS_SETTINGS_HDD}"; fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Finalise chroot configuration...                                     -"
echo "------------------------------------------------------------------------"

arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --target=i386-pc ${PS_HDD}
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
echo -e "y\ny" | (arch-chroot /mnt pacman -Scc)

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cleanup...                                                           -"
echo "------------------------------------------------------------------------"

if [ "${PS_HDD_FILESYSTEM,,}" == "btrfs" ]; then
  sync
  umount /mnt/home
  umount /mnt
  mount -o space_cache=${PS_HDD_BTRFS_SPACE_CACHE},compress=no ${PS_HDD_PARTITION} /mnt
elif [ "${PS_HDD_FILESYSTEM,,}" == "ext4" ]; then
  e4defrag ${PS_HDD_PARTITION}
fi

sync
umount /mnt

if [ "${PS_HDD_FILESYSTEM,,}" == "btrfs" ]; then btrfs check ${PS_HDD_PARTITION}         ; fi
if [ "${PS_HDD_FILESYSTEM,,}" == "ext4"  ]; then e2fsck -fyvD ${PS_HDD_PARTITION} || true; fi
if [ "${PS_HDD_FILESYSTEM,,}" == "ext4"  ]; then e2fsck -fyv ${PS_HDD_PARTITION}         ; fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
