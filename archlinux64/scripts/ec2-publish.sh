#!/bin/bash -e
#
# A set of scripts to prepare a Virtual Machine to become an Archlinux image.
# Starting with just the Arch Linux ISO this set of scripts will install
# Arch Linux and make any required configuration to the image.
#
# EC2 publish: publishes an artifact to S3 so it can later be converted to an
# EC2 AMI.
#
###############################################################################

set -o pipefail

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Arch Linux Cloud Image Preparation Script                            -"
echo "------------------------------------------------------------------------"
echo "- EC2 Publish: Copy artifact to S3                                     -"
echo "------------------------------------------------------------------------"

hash aws

while getopts "b:k:r:s:" PS_OPT; do
  case ${PS_OPT} in
    b)
      PS_PUBLISH_BUCKET="${OPTARG}"
      ;;
    k)
      PS_PUBLISH_KEY="${OPTARG}"
      ;;
    r)
      PS_PUBLISH_ROLE="${OPTARG}"
      ;;
    s)
      PS_PUBLISH_SOURCE="${OPTARG}"
      ;;
  esac
done

if [ -z "${PS_PUBLISH_BUCKET}" ]; then echo "FAIL: No destination S3 bucket specified (-b)"     ; exit 1; fi
if [ -z "${PS_PUBLISH_KEY}"    ]; then echo "FAIL: No destination S3 bucket key specified! (-k)"; exit 1; fi
if [ -z "${PS_PUBLISH_ROLE}"   ]; then echo "FAIL: No IAM role to assume specified (-r)"        ; exit 1; fi
if [ -z "${PS_PUBLISH_SOURCE}" ]; then echo "FAIL: No source artifact to upload specified (-s)" ; exit 1; fi

echo "Source Artifact: ${PS_PUBLISH_SOURCE}"
echo ""

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Assuming role...                                                     -"
echo "------------------------------------------------------------------------"

PS_PUBLISH_STS="$(aws sts assume-role                                                                                                                                                                                   \
                    --role-arn "${PS_PUBLISH_ROLE}"                                                                                                                                                                     \
                    --role-session-name "packer-ec2-publish"                                                                                                                                                            \
                    --query 'join(``, [`export AWS_ACCESS_KEY_ID=`, Credentials.AccessKeyId, `; export AWS_SECRET_ACCESS_KEY=`, Credentials.SecretAccessKey, `; export AWS_SESSION_TOKEN=`, Credentials.SessionToken])' \
                    --output text)"

eval "${PS_PUBLISH_STS}"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Publishing to S3...                                                  -"
echo "------------------------------------------------------------------------"

echo "Uploading to S3: ${PS_PUBLISH_SOURCE}"
aws s3 cp --quiet "${PS_PUBLISH_SOURCE}" "s3://${PS_PUBLISH_BUCKET}/${PS_PUBLISH_KEY}"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cleaning source artifact...                                          -"
echo "------------------------------------------------------------------------"

rm -rfv "${PS_PUBLISH_SOURCE}"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
