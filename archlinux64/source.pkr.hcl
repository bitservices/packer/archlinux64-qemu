###############################################################################

variable "source_cpu"         { default = "host" }
variable "source_efi"         { default = false  }
variable "source_tpm"         { default = false  }
variable "source_cores"       { default = 2      }
variable "source_chipset"     { default = "q35"  }
variable "source_headless"    { default = true   }
variable "source_memory_mb"   { default = 2048   }
variable "source_accelerator" { default = "kvm"  }

###############################################################################

variable "source_boot_wait_primary"   { default = "5s"  }
variable "source_boot_wait_secondary" { default = "30s" }

###############################################################################

variable "source_iso_arch"           { default = "x86_64"                                                           }
variable "source_iso_card"           { default = "ide"                                                              }
variable "source_iso_date"           { default = "2025.02.01"                                                       }
variable "source_iso_mirror"         { default = "https://geo.mirror.pkgbuild.com"                                  }
variable "source_iso_checksum_type"  { default = "sha256"                                                           }
variable "source_iso_checksum_value" { default = "45f097416d604ac20e982d2137534ffbe00990ba36dcb9bd259c05a4515f20cd" }

###############################################################################

variable "source_network_card" { default = "virtio-net" }

###############################################################################

variable "source_shutdown_command" { default = "systemctl poweroff" }
variable "source_shutdown_timeout" { default = "30s"                }

###############################################################################

variable "source_ssh_timeout"  { default = "1m"               }
variable "source_ssh_username" { default = "root"             }
variable "source_ssh_password" { default = "pbSlHhsOc4RD79Ja" } # Not a secret, used and thrown away during build

###############################################################################

variable "source_storage_card"     { default = "virtio"    }
variable "source_storage_cache"    { default = "writeback" }
variable "source_storage_format"   { default = "qcow2"     }
variable "source_storage_compact"  { default = true        }
variable "source_storage_discard"  { default = "unmap"     }
variable "source_storage_compress" { default = true        }

###############################################################################

variable "source_video_card"    { default = "virtio" }
variable "source_video_display" { default = "none"   }

###############################################################################

locals {
  source_iso_url      = format("%s/iso/%s/archlinux-%s-%s.iso", var.source_iso_mirror, var.source_iso_date, var.source_iso_date, var.source_iso_arch)
  source_iso_checksum = format("%s:%s", var.source_iso_checksum_type, var.source_iso_checksum_value)

  source_boot_command = [
    format("<enter><wait%s>", var.source_boot_wait_secondary)                                    ,
    format("echo -e '%s\\n%s' | passwd<enter>", var.source_ssh_password, var.source_ssh_password),
    "systemctl start sshd.service<enter>"
  ]
}

###############################################################################
