###############################################################################

packer {
  required_plugins {
    qemu = {
      source = "github.com/hashicorp/qemu"
      version = "=1.1.0"
    }

    vagrant = {
      source = "github.com/hashicorp/vagrant"
      version = "=1.1.4"
    }
  }
}

###############################################################################
