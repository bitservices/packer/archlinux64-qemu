###############################################################################

source "qemu" "ec2" {
  vga              = var.source_video_card
  cpus             = var.source_cores
  vtpm             = var.source_tpm
  format           = var.source_storage_format
  memory           = var.source_memory_mb
  display          = var.source_video_display
  iso_url          = local.source_iso_url
  vm_name          = local.ec2_name
  efi_boot         = var.source_efi
  headless         = var.source_headless
  boot_wait        = var.source_boot_wait_primary
  cpu_model        = var.source_cpu
  disk_size        = var.ec2_storage_root_size
  disk_cache       = var.source_storage_cache
  net_device       = var.source_network_card
  accelerator      = var.source_accelerator
  ssh_timeout      = var.source_ssh_timeout
  boot_command     = local.source_boot_command
  ssh_password     = var.source_ssh_password
  ssh_username     = var.source_ssh_username
  iso_checksum     = local.source_iso_checksum
  disk_discard     = var.source_storage_discard
  machine_type     = var.source_chipset
  disk_interface   = var.source_storage_card
  cdrom_interface  = var.source_iso_card
  skip_compaction  = ! var.source_storage_compact
  disk_compression = var.source_storage_compress
  output_directory = local.ec2_output
  shutdown_command = var.source_shutdown_command
  shutdown_timeout = var.source_shutdown_timeout
}

###############################################################################
