# archlinux64

An Archlinux x86-64 image with a focus on being as automation-friendly as
possible. The aim is for this image to 'just work' on any platform with no
special bootstrapping or manual changes over SSH. As soon as the instance is
started it is ready for immediate provisioning with your chosen configuration
management tool.

This image is also aimed to be flexible. That means it is as close to bare
Archlinux as possible with just enough for it to work on cloud platforms
and for configuration management solutions to provision the instance further.
Decisions have been made with the design of this image to *try* to make it
integrate easily into complex environments, such as Vagrant setups that have
a complex `Vagrantfile`.

Creation of this image is done by scripts that are ran on the base VM after it
has been booted from the Archlinux ISO. Every release is based on a completely
fresh hard disk image.

These scripts publish a Vagrant optimised copy of this image to Vagrant Cloud
and an Amazon EC2 optimised copy to an S3 bucket.

-------------------------------------------------------------------------------

## Live Image Identifiers

* **Vagrant**: bitservices/archlinux64

-------------------------------------------------------------------------------

## User Accounts

This image comes with the following user accounts configured:

* **root**: Password authentication disabled and no SSH authorised keys.
* **archlinux**: Password authentication disabled, SSH key to be injected by cloud-init.

-------------------------------------------------------------------------------

## NoCloud ISO for Vagrant

To use the Vagrant version of this image you must first generate a NoCloud ISO
to configure some base settings, such as the **archlinux** users SSH key.

### Generate the ISO

1. Create a temporary folder to work in, for example: `/tmp/nocloud`

2. Create the **meta-data** file, `/tmp/nocloud/meta-data`. Assign an
appropiate instance ID and host name. Example:
```
instance-id: iid-nocloud01
local-hostname: archlinux
```

3. Create the **user-data** file, `/tmp/nocloud/user-data`. List the
SSH key(s) you will use to connect to the Vagrant instance. Example:
```
#cloud-config
ssh_authorized_keys:
  - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIxIj/rN51r+KP5govyEw7RcBZyOLmrAs96aSu8bY7iG example@vagrant
```

4. Create an ISO file from the temporary folder:
```
mkisofs --joliet --rock --volid CIDATA -o "nocloud.iso" "/tmp/nocloud"
```

5. Move the ISO to a valid libvirt storage pool that QEMU has permission to
use. I am not going to document exactly how that is done because I am quite new
to libvirt and QEMU so am not fully clear on the best way to do it. For the
purpose of this example, we will assume the ISO has been moved to the following
location: `/var/lib/libvirt/images/nocloud.iso`.

### Setup Vagrant

1. Initialise a Vagrant environment, if you have not yet done so:
```
vagrant init bitservices/archlinux64
```

2. Apply the following modifications do the `Vagrantfile` to make sure the
NoCloud ISO is mounted and the SSH key you specified is actually used:
```
Vagrant.configure("2") do |config|
  config.vm.provider :libvirt do |libvirt|
    libvirt.storage :file, :device => :cdrom, :path => '/var/lib/libvirt/images/nocloud.iso'
  end

  config.ssh.keys_only  = false
  config.ssh.insert_key = false
end
```

3. You should now be able to bring up the VM:
```
vagrant up --provider=libvirt
```

-------------------------------------------------------------------------------

## Summary of Changes from base Archlinux

* **Root Filesystem Type**: BTRFS
    * **BTRFS Configuration**: 
        * **Compression**: zstd
        * **Label**: `ARCH_SYSTEM`
        * **Options**:
            * discard
            * ssd
        * **Space Cache**: v2
        * **Subvolumes**:
            * `@` **/**
            * `@home` **/home**
    * **EXT4 Configuration**:
        * **Check Interval**: `1m` (every 1 month)
        * **Label**: `ARCH_SYSTEM`
        * **Options**:
            * discard
* **Packages**
    * **Pacman Options**:
        * Enabled `Color`
        * Enabled `ParallelDownloads` and set to `3`
    * **Keys**:
        * Added `7FC5C97F4044C19A` (bitservices-packages.asc)
    * **Repositories**:
        * core
        * extra
        * community
        * bitservices (custom, signed)
    * **Mirrors**: Standard Archlinux mirrorlist with only **https** enabled **Worldwide** servers enabled
    * **Base Packages**: (In addition to `base`)
        * linux
        * linux-firmware
        * linux-headers
        * cloud-init
        * cloud-guest-utils
        * logrotate
        * grub
        * pacman-contrib
        * mdadm
        * networkmanager
        * net-tools
        * inetutils
        * sudo
        * openssh
        * btrfs-progs
        * f2fs-tools
        * dosfstools
        * python
        * vim
        * bitservices-core (bitservices)
        * pretty-motd (bitservices)
    * **EC2 AMI Packages**:
        * prometheus-node-exporter
        * aws-cli-v2-bits (bitservices)
    * **Cache**: Cleared before image capture
* **FSTAB**: Generated using `genfstab` using UUIDs
* **Hostname**: archlinux
* **Locale**:
    * **Language**: `en_GB.UTF-8`
    * **Keymap**: `uk`
* **Logrotate**: Enable `compress`
* **Ramdisk**:
    * Disable fallback ramdisk
    * Include `mdmon` binary
    * Include `/etc/os-release` file
    * Include `systemd` hook
    * Move `block` hook to directly after `systemd`
    * Include `mdadm_udev` hook
    * Include `sd-vconsole` hook
    * Remove `udev` hook
    * Remove `autodetect` hook
    * Remove `microcode` hook
    * Remove `keymap` hook
    * Remove `consolefont` hook
* **Device Manager**
    * **EC2 Only Device Manager Changes**:
        * Include Amazon EBS friendly name rules for NVMe disks
* **Cloud Init**
    * **Disable Root**: Enabled
    * **Preserve Hostname**: Disabled
    * **Resize Root Filesystem**: Enabled
    * **Distribution**: arch
    * **Default User**: archlinux
        * **Friendly Name**: Archlinux
        * **Shell**: /bin/bash
        * **Sudo**: ALL=(ALL) NOPASSWD:ALL
        * **Lock Password**: Enabled
        * **Groups**:
            * adm
            * audio
            * floppy
            * users
            * uucp
            * video
            * wheel
    * **SSH Settings**:
        * **Service Name**: sshd
        * **Password Auth**: Disabled
        * **Generated SSH Host Key Types**: ed25519
    * **Modules**:
        * **Init**: `seed_random`
        * **Init**: `growpart`
        * **Init**: `resizefs`
        * **Init**: `bootcmd`
        * **Init**: `write-files`
        * **Init**: `mounts`
        * **Init**: `set_hostname`
        * **Init**: `update_hostname`
        * **Init**: `users-groups`
        * **Init**: `ssh`
        * **Config**: `set-passwords`
        * **Config**: `runcmd`
        * **Final**: `rightscale_userdata`
        * **Final**: `scripts-per-once`
        * **Final**: `scripts-per-boot`
        * **Final**: `scripts-per-instance`
        * **Final**: `scripts-user`
        * **Final**: `ssh-authkey-fingerprints`
        * **Final**: `keys-to-console`
        * **Final**: `final-message`
    * **Networking**: Management by cloud-init disabled
    * **Datasources**:
        * NoCloud
        * Ec2
        * None
* **Networking**
    * **Policies**:
        * **Device Naming**: keep, kernel, database, onboard, slot, path
        * **Alternate Device Naming**: database, onboard, slot, path
        * **IPv6 Privacy**: Enabled (default)
        * **MAC Address**: None (kernel defaults)
* **Time**
    * **NTP**: Enabled
    * **EC2 Only Time Changes**:
        * Use `169.254.169.123` NTP server by default.
* **Services**
    * Enabled `pretty-motd.service`
    * Enabled `sshd.service`
    * Enabled `systemd-timesyncd.service`
    * Enabled `NetworkManager.service`
    * Enabled `cloud-init-main.service`
    * Enabled `cloud-init-local.service`
    * Enabled `cloud-init-network.service`
    * Enabled `cloud-config.service`
    * Enabled `cloud-final.service`
    * Enabled `prometheus-node-exporter.service` (EC2 AMI only)
* **Bootloader**
    * Grub with default settings
        * **Target**: i386-pc
        * **Device**: /dev/sda
* **User Accounts**
    * Root user is locked
    * Further user accounts configured with `cloud-init`
* **Metrics - EC2 AMI Only**
    * **Enabled Node Exporter Collectors**:
        * btrfs
        * cpu
        * cpufreq
        * diskstats
        * edac
        * entropy
        * filesystem
        * hwmon
        * loadavg
        * mdadm
        * meminfo
        * netclass
        * netdev
        * netstat
        * nfs
        * nvme
        * pressure
        * stat
        * thermal_zone
        * udp_queues

-------------------------------------------------------------------------------

## Base Virtual Machine Specification

The virtual machine specification is as follows. If a configuration item is not
listed below the QEMU default will apply. The QEMU builder only outputs the
disk as an artefact meaning the builder machines configuration is much less
relevant compared to VirtualBox.

* **Name**: archlinux64
* **Virtualisation**:
    * **Accelerator**: KVM (TCG in CI)
* **Firmware**: BIOS
* **Chipset**:
    * **Type**: Q35
* **Processor**:
    * **Count**: 2
    * **Type**: Host (qemu64 in CI)
* **Base Memory**: 2048mb (5120mb in CI)
* **Storage Controller**:
    * **Cache**: writeback
    * **Compression**: Enabled
    * **Discard**: Enabled
    * **Disk**: 3GB (ec2) 20GB (vagrant)
    * **Format**: qcow2
    * **Type**: virtio
* **ISO Controller**:
    * **Type**: IDE
* **Network Adapters**:
    * **Adapter 1**: User, virtio-net
* **Video**:
    * **Adapter**: virtio
* **TPM**: Disabled

When using Vagrant, you must configure your own specification by modifying your
`Vagrantfile`. Failure to do so will use Vagrant defaults for the `libvirt`
provider and **NOT** the specification listed here.

-------------------------------------------------------------------------------

## How to 'Roll Your Own'

The process of creating this image has been automated as much as possible and
all code used in doing so has been uploaded into this repository.

In order to create this image on your own system, whether it be you would like
to customise the image or just for piece of mind, the process is as follows:

### Prerequisites

You will need the following:

* Access to a Linux Desktop with standard tools available such as Bash and Git.
* QEMU: https://www.qemu.org/
* Packer from Hashicorp: https://www.packer.io/
* AWS CLI (not required if skipping ec2 build): https://aws.amazon.com/cli/

### Creating Images

1. Clone the git repository:
```
git clone https://gitlab.com/bitservices/packer/archlinux64-qemu.git
```

2. If you are behind a proxy, you will need to set two variables so Packer can
pass these into the virtual machine. If you are directly connected to the
Internet please skip this step!
```
# Replace the host and port below to match your proxy settings.
export PKR_VAR_proxy_host="your.proxy.server"
export PKR_VAR_proxy_port="8080"
```

3. Make sure you are in the correct folder to run Packer:
```
cd archlinux64-qemu
```

4. Make any changes you wish to make to the packer HCL or the supporting
scripts. At a minimum you will need to change:
```
archlinux64/main.pkr.hcl: output       - to a place to output artefacts, ideally somewhere with plenty of space but temporary
archlinux64/main.pkr.hcl: release      - to a version number for the Vagrant box / S3 bucket artefact in format 0.0.0
archlinux64/main.pkr.hcl: ec2_role     - to the ARN of an IAM role that has permission to upload objects to 'ec2_bucket'
archlinux64/main.pkr.hcl: ec2_bucket   - to an S3 bucket that you have permission to write to
archlinux64/main.pkr.hcl: vagrant_repo - to a Vagrant Cloud repository that you have permission to write to
```

5. Make sure your AWS CLI configuration is correct. Make sure the following
variables are set:
```
AWS_PROFILE        : If you need to assume a role in order to write the ec2 artefact to S3
VAGRANT_CLOUD_TOKEN: To store & release the Vagrant image in your Vagrant Cloud account
```

6. Run Packer against `archlinux64`:
```
packer build archlinux64
```

7. Observe any output or errors. If everything is successful the newly created
images will be uploaded to Vagrant Cloud and to Amazon S3.
